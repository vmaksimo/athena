/*
  Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration
*/

#include "src/InDetToXAODClusterConversion.h"
#include "src/XAODToInDetClusterConversion.h"

DECLARE_COMPONENT( InDet::InDetToXAODClusterConversion )
DECLARE_COMPONENT( InDet::XAODToInDetClusterConversion )

